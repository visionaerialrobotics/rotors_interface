/*!*******************************************************************************
 *  \brief      This is the command interface package for Rotors Simulator.
 *  \authors    Ramon Suarez, Hriday Bavle, Alberto Rodelgo
 *  \copyright  Copyright (c) 2020 Universidad Politecnica de Madrid
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

#include <thread>
#include <iostream>
#include <math.h>
#include <cmath>

//tf messages
#include <tf/transform_datatypes.h>

//// ROS  ///////
#include "ros/ros.h"

#include <robot_process.h>
#include "communication_definition.h"
#include "droneMsgsROS/droneAutopilotCommand.h"
#include "droneMsgsROS/battery.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneStatus.h"


//geometry msgs
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/PoseStamped.h"

//Navmsg
#include "nav_msgs/Odometry.h"

//mav_msgs_rotors
#include "mav_msgs_rotors/RollPitchYawrateThrust.h"
#include "mav_msgs/RollPitchYawrateThrust.h"
#include "eigen_conversions/eigen_msg.h"

//standard messages ROS
#include "std_msgs/Float64.h"
#include "tf/transform_datatypes.h"
#include "eigen_conversions/eigen_msg.h"
#include "Eigen/Core"
#include "Eigen/Geometry"
#include "tf_conversions/tf_eigen.h"
#include <cmath>

//Mavros
#include "mavros_msgs/AttitudeTarget.h"
#include "mavros_msgs/ActuatorControl.h"
#include "mavros_msgs/State.h"
#include "mavros/mavros_uas.h"
#include <pluginlib/class_list_macros.h>
#include "cvg_string_conversions.h"
#include <mavros/mavros_plugin.h>
#include <mavros/setpoint_mixin.h>

class QuadrotorCommandInterface : public RobotProcess
{
    //Constructors and destructors
public:
    QuadrotorCommandInterface();
    ~QuadrotorCommandInterface();
    int getRate();
protected:
    bool resetValues();    
private: /*RobotProcess*/
    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();

protected:
    //Subscribers
    ros::Subscriber ML_autopilot_command_subscriber;
    ros::Subscriber ML_autopilot_command_subscriber2;    
    mav_msgs::RollPitchYawrateThrust msg_generated_by_quadrotor_PID_controller;
    void MLAutopilotCommandCallback(const mav_msgs::RollPitchYawrateThrust& msg);
    void StatusCallback(const droneMsgsROS::droneStatus& msg);    
    void QuadrotorCommandCallback(const mav_msgs::RollPitchYawrateThrust& msg);
    // Publishers
    ros::Publisher drone_CtrlInput_publisher;

public:
    std::string rotors_drone_model;
    std::string drone_namespace;    
    int rotors_drone_id;
    int frecuency;

    std::vector<geometry_msgs::PoseStamped> poses;
    geometry_msgs::Quaternion orientation;
    Eigen::Quaterniond quaterniond, quaterniond_transformed;
    double roll_command, pitch_command, yaw_command, yaw_angle;
    double roll, pitch, yaw;
    double battery_percent;
    double yawRef, yaw_initial_mag, ekf_yaw;
    double timePrev, timeNow;
    bool update;
};
