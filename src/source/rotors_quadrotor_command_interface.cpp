/*!*******************************************************************************
 *  \brief      This is the command interface package for Rotors Simulator.
 *  \authors    Ramon Suarez, Hriday Bavle, Alberto Rodelgo
 *  \copyright  Copyright (c) 2020 Universidad Politecnica de Madrid
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

#include "rotors_quadrotor_command_interface.h"

using namespace std;

QuadrotorCommandInterface::QuadrotorCommandInterface()
{
}

QuadrotorCommandInterface::~QuadrotorCommandInterface()
{
}

void QuadrotorCommandInterface::ownSetUp()
{
    ros::param::get("~rotors_drone_id", rotors_drone_id);
    ros::param::get("~rotors_drone_model", rotors_drone_model);
    ros::param::get("~frecuency", frecuency);                      
}

void QuadrotorCommandInterface::ownStart()
{
    ros::NodeHandle n;    
    //Publishers
    drone_CtrlInput_publisher       = n.advertise<mav_msgs_rotors::RollPitchYawrateThrust>("/"+rotors_drone_model+std::to_string(rotors_drone_id)+"/command/roll_pitch_yawrate_thrust", 1, true);

    //Subscribers
    ML_autopilot_command_subscriber = n.subscribe("actuator_command/roll_pitch_yaw_rate_thrust", 1, &QuadrotorCommandInterface::MLAutopilotCommandCallback, this);
}

//Reset
bool QuadrotorCommandInterface::resetValues()
{
    return true;
}

int QuadrotorCommandInterface::getRate(){
    return frecuency;
}

//Stop
void QuadrotorCommandInterface::ownStop()
{
    drone_CtrlInput_publisher.shutdown();   
    ML_autopilot_command_subscriber.shutdown();
}

//Run
void QuadrotorCommandInterface::ownRun()
{

}

#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void QuadrotorCommandInterface::MLAutopilotCommandCallback(const mav_msgs::RollPitchYawrateThrust& msg)
{
          drone_CtrlInput_publisher.publish(msg);
}

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, "QuadrotorCommandInterface");

    cout<<"[ROSNODE] Starting QuadrotorCommandInterface"<<endl;

    //Vars
    QuadrotorCommandInterface quadrotor_comman_interface;
    quadrotor_comman_interface.setUp();
    quadrotor_comman_interface.start();

    ros::Rate loop_rate(quadrotor_comman_interface.getRate());
 
    try
    {    
        while(ros::ok())
        {  
             ros::spinOnce();
             loop_rate.sleep();
        }
    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }
}